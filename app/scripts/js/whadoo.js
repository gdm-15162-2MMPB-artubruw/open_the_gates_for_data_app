(function($) {
    "use strict"; // Start of use strict

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('.page-scroll a').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    // Highlight the top nav as scrolling occurs
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 51
    });

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a:not(.dropdown-toggle)').click(function() {
        $('.navbar-toggle:visible').click();
    });

    // Offset for Main Navigation
    $('#mainNav').affix({
        offset: {
            top: 100
        }
    });

    // Floating label headings for the contact form
    $(function() {
        $("body").on("input propertychange", ".floating-label-form-group", function(e) {
            $(this).toggleClass("floating-label-form-group-with-value", !!$(e.target).val());
        }).on("focus", ".floating-label-form-group", function() {
            $(this).addClass("floating-label-form-group-with-focus");
        }).on("blur", ".floating-label-form-group", function() {
            $(this).removeClass("floating-label-form-group-with-focus");
        });
    });
    $('.nav-bars').click(function(){
        $('.filter-card').toggleClass('display-block');
    });

    var rangeSlider = function(){
        var slider = $('.range-slider'),
            range = $('.range-slider__range'),
            value = $('.range-slider__value');

        slider.each(function(){

            value.each(function(){
                var value = $(this).prev().attr('value');
                $(this).html(value);
            });

            range.on('input', function(){
                $(this).next(value).html(this.value);
            });
        });
    };

    rangeSlider();

    //Eerste JSON-lading (weersvoorspelling)
    /*(function() {
        var App1 = {
            "init": function() {
                this.URLDSGENTFORECAST = 'http://datatank.stad.gent/4/milieuennatuur/weersverwachting1u.json';
                this.gentWeersvoorspellingData = null;
                this.loadGentWeersvoorspelling();
            },
            "loadGentWeersvoorspelling": function() {

                var self = this;

                getJSONByPromise(this.URLDSGENTFORECAST).then(
                    function(data) {
                        self.gentWeersvoorspellingData = data;
                        self.updateUI();
                    },
                    function(status) {
                        console.log(status);
                    }
                );
            },
            "updateUI": function() {
                if(this.gentWeersvoorspellingData != null) {
                    var tempStr = '', gentWeersvoorspelling = null;
                    tempStr += '<div class="forecast">';
                    for (var i = 0; i < this.gentWeersvoorspellingData.properties.attributes.length; i++) {
                        gentWeersvoorspelling = this.gentWeersvoorspellingData.properties.attributes[i];
                        tempStr += '<div class="cards">';
                        tempStr += '<span class="forecast-name">' + gentWeersvoorspelling.attributeName + '</span>' + '<br>';
                        tempStr += '<span class="forecast-value">' + gentWeersvoorspelling.value + '</span>' + '<br>';
                        tempStr += '</div>'
                    }
                    tempStr += '</div>';
                    document.querySelector('.jsondata-forecast').innerHTML = tempStr;
                }
            }
        };

        App1.init();

    })();*/

    (function () {
        var App1 = {
            "init": function () {
                this.URLDSGENTFORECAST = 'http://datatank.stad.gent/4/milieuennatuur/weersverwachting1u.json';
                this.gentWeersvoorspellingData = null;
                this.loadGentWeersvoorspelling();
            },
            "loadGentWeersvoorspelling": function () {
                var self = this;
                getJSONByPromise(this.URLDSGENTFORECAST).then(
                    function (data) {
                        self.gentWeersvoorspellingData = data;
                        self.updateUI();
                    },
                    function (status) {
                        console.log(status);
                    }
                );
            },
            "updateUI": function () {
                if (this.gentWeersvoorspellingData != null) {
                    var tempStr = '', gentWeersvoorspelling = null, gentWeersvoorspelling_temperatuur = null, gentWeersvoorspelling_gevtemp = null, gentWeersvoorspelling_neerslag = null, gentWeersvoorspelling_windsnelheid = null, gentWeersvoorspelling_algemeen = null;
                    tempStr += '<div class="forecast">';

                    gentWeersvoorspelling_temperatuur = this.gentWeersvoorspellingData.properties.attributes[0];
                    gentWeersvoorspelling_gevtemp = this.gentWeersvoorspellingData.properties.attributes[1];
                    gentWeersvoorspelling_neerslag = this.gentWeersvoorspellingData.properties.attributes[2];
                    gentWeersvoorspelling_windsnelheid = this.gentWeersvoorspellingData.properties.attributes[4];
                    gentWeersvoorspelling_algemeen = this.gentWeersvoorspellingData.properties.attributes[6];

                    tempStr += '<span class="forecast-value">' + '<div class="forecast-name-general">' + gentWeersvoorspelling_algemeen.value + '</div>' + '</span>' + '<br>';
                    tempStr += '<span class="forecast-value">' + '<div class="forecast-name">' + 'TEMPERATUUR: ' + '</div>' + '<div class="forecast-value2">' + gentWeersvoorspelling_temperatuur.value + '°C</div></span>' + '<br>';
                    tempStr += '<span class="forecast-value">' + '<div class="forecast-name">' + 'GEVOELSTEMPERATUUR: ' + '</div>' + '<div class="forecast-value2">' + gentWeersvoorspelling_gevtemp.value + '°C</div></span>' + '<br>';
                    tempStr += '<span class="forecast-value">' + '<div class="forecast-name">' + 'NEERSLAG: ' + '</div>' + '<div class="forecast-value2">' + gentWeersvoorspelling_neerslag.value + 'mm</div></span>' + '<br>';
                    tempStr += '<span class="forecast-value">' + '<div class="forecast-name">' + 'WINDSNELHEID: ' + '</div>' + '<div class="forecast-value2">' + gentWeersvoorspelling_windsnelheid.value + ' km/h</div></span>' + '<br>';

                    tempStr += '</div>';
                    document.querySelector('.jsondata-forecast').innerHTML = tempStr;
                }
            }
        };
        App1.init();
    })();


})(jQuery); // End of use strict
