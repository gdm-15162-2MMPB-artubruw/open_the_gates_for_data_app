/**
 * Created by Artuur on 6/08/16.
 */

// algemene variabelen die gebruikt worden in verschillende functies
var mapObject;
var mapLoader;
var userlat = 0;
var userLong = 0;
var circle;
var markers = [];
var returnPolyLines = [];
var returnMultiLines = [];
var jsonLinks = [];

// dit alles wordt uitgevoerd wanneer de browser jQuery start
$(document).ready(function(){

    //als de locatie van de gebruiker is gevonden, creeëren we de map en plaatsen we een marker
    function geolocationSuccess(position) {
        //userLat = position.coords.latitude;
        //userLong = position.coords.longitude;

        //tijdelijke code (set location in Gent)
        userLat = 51.048764061465;
        userLong = 3.7337818242073;
        var userLatLng = new google.maps.LatLng(userLat, userLong);

        mapLoader = $('#mapLoader');

        var myOptions = {
            zoom : 12,
            center : userLatLng,
            mapTypeId : google.maps.MapTypeId.ROADMAP
        };

        // teken de map
        mapObject = new google.maps.Map(document.getElementById("map"), myOptions);

        // plaats de marker
        new google.maps.Marker({
            map: mapObject,
            position: userLatLng,
            icon: 'images/icons/gmaps_iconME.png'
        });

        // Cirkel rond de gebruiker, radius = 0. Enkel bij verslepen slider vergroot de radius.
        // de cirkel wordt ook gebruikt om te zien als de coordinaten binnen de radius zijn.
        circle = new google.maps.Circle({
            center: new google.maps.LatLng(userLat, userLong),
            radius: parseInt($(".range-slider__range").val(), 10),
            map: mapObject,
            fillColor: '#CCCC52',
            fillOpacity: 0.24,
            strokeColor: '#192B33',
            strokeOpacity: 0.5
        });

        mapLoader.fadeOut();
    }

    // als de gebruiker zijn locatie weigert te geven, wordt een popup gestuurd
    function geolocationError(positionError) {
        alert("Om deze app te gebruiken hebben we toegang nodig tot uw locatie!")
    }

    // ophalen locatie gebruiker
    function geolocateUser() {
        // als de browser GEOloc API ondersteund
        if (navigator.geolocation)
        {
            var positionOptions = {
                enableHighAccuracy: true,
                timeout: 10 * 1000 // 10 seconds
            };
            navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError, positionOptions);
        }
        else
            alert("Uw browser laat niet toe om uw locatie op te vragen.")
    }

    // de JSONlinks array wordt ingevuld met verschillende urls met elk een specifiek kleur of marker.
    function pushJSONLinks() {
        jsonLinks.push(["http://datatank.stad.gent/4/cultuursportvrijetijd/bioscopen.geojson", "images/icons/gmaps_icon_cinema2.png"]);
        jsonLinks.push(["http://datatank.stad.gent/4/cultuursportvrijetijd/speelterreinen.geojson", "#85449d"]);
        jsonLinks.push(["http://datatank.stad.gent/4/milieuennatuur/parken.geojson", "#c6e041"]);
        jsonLinks.push(["http://datatank.stad.gent/4/werkeneconomie/winkellussen.geojson", "#004c63"]);
        jsonLinks.push(["http://datatank.stad.gent/4/cultuursportvrijetijd/sportcentra.geojson", "images/icons/gmaps_icon_sportcentra.png"]);
    }

    // Deze functie maakt de markers
    // We halen de data van de url en loopen door het resultaat tot we de coordinaten krijgen
    // deze coordinaten worden gesaved als google.maps.LatLng en geplaatst op de map
    function setMarkers(jsonLink) {
        var radius = $("#searchRange").text;
        var coords = [];
        var i = 0;
        $.getJSON(jsonLink[0], function( data ) {
            //console.log(data);
            //als het resultaat meer dan 0 resultaten heeft
            if(data['coordinates'].length > 0){

                //voor elke coordinaat terug is gevonden
                for (i = 0; i < data['coordinates'].length; i++) {
                    coords.push(data['coordinates'][i]);
                }

                //reset de foreach teller
                i = 0;

                for (i = 0; i < coords.length; i++) {

                    var cinemaLatLong = new google.maps.LatLng(coords[i][1], coords[i][0]);
                    //check if marker is within radius
                    if(circle.contains(cinemaLatLong)){
                        markers.push(new google.maps.Marker({
                            map: mapObject,
                            position: cinemaLatLong,
                            icon: jsonLink[1]
                        }));
                    }
                }
            }

            mapLoader.fadeOut();
        });
    }
    // Deze functie tekent de polygons.
    // elk punt in de lijn is gesaved als google.maps.LatLng en worden gebundeld om een polygon te tekenen
    function setPolygons(jsonLink) {
        var radius = $("#searchRange").text;
        var polyLines = [];

        $.getJSON(jsonLink[0], function(data) {
            //console.log(data);
            var linesCoords = data['coordinates'];

            for(i = 0; i < linesCoords.length; i++) {
                polyLines[i] = [];
                var singleLine = linesCoords[i];
                var coords = singleLine[0];
                for(j = 0; j < coords.length; j++) {
                    var coord = coords[j];
                    var latlng = new google.maps.LatLng(coord[1], coord[0]);
                    if(circle.contains(latlng)) {
                        polyLines[i].push(latlng);
                    }

                }
            }

            for(k = 0; k < polyLines.length; k++) {
                var line = polyLines[k];
                returnPolyLines.push(new google.maps.Polygon({
                    path: line,
                    strokeColor: jsonLink[1],
                    strokeWeight: 2,
                    fillColor: '#225588'
                }));
            }

            for(l = 0; l < returnPolyLines.length; l++) {
                returnPolyLines[l].setMap(mapObject);
            }
        });
    }

    // lopen door de array en tekenen van lijn mbv coordinaten
    function setMultiLinks(jsonLink) {
        var radius = $("#searchRange").text;
        var multiLines = [];

        $.getJSON(jsonLink[0], function(data) {
            //console.log(data);
            var multiLinesCoords = data['coordinates'];

            for(m = 0; m < multiLinesCoords.length; m++) {
                multiLines[m] = [];
                var singleLine = multiLinesCoords[m];
                for(n = 0; n < singleLine.length; n++) {
                    var coord = singleLine[n];
                    var latlng = new google.maps.LatLng(coord[1], coord[0]);
                    if(circle.contains(latlng)) {
                        multiLines[m].push(latlng);
                    }

                }
            }

            for(o = 0; o < multiLines.length; o++) {
                var line = multiLines[o];
                returnMultiLines.push(new google.maps.Polygon({
                    path: line,
                    strokeColor: jsonLink[1],
                    strokeWeight: 2,
                }));
            }

            for(p = 0; p < returnMultiLines.length; p++) {
                returnMultiLines[p].setMap(mapObject);
            }
        });
    }

    // wanneer de slider beweegt, wordt de waarde van de radius van de cirkel en de map z'n bounds aangepast
    $(".range-slider__range").change(function() {
        circle.setRadius(parseInt($(".range-slider__range").val(), 10));
        mapObject.fitBounds(circle.getBounds());
    });

    // wanneer de zoekknop is ingedrukt, clearen we de markers, polygons en lijnen.
    // we checken welke checkboxes gecheckt zijn en roepen de juiste functie aan
    $("#setFilterBtn").click(function() {

        mapLoader.fadeIn('fast');

        $('.filter-card').removeClass('display-block');

        var markerLength = markers.length;
        for (a = 0; a < markerLength; a++) {
            markers[a].setMap(null);
        }
        markers = [];

        var polyLength = returnPolyLines.length;
        for (b = 0; b < polyLength; b++) {
            returnPolyLines[b].setMap(null);
        }
        returnPolyLines = [];

        var multiLength = returnMultiLines.length;
        for (c = 0; c < multiLength; c++) {
            returnMultiLines[c].setMap(null);
        }
        returnMultiLines = [];

        if ($("#fltBioscopen").is(':checked')) {
            setMarkers(jsonLinks[0]);
        }

        if ($("#fltSpeel").is(':checked')) {
            setPolygons(jsonLinks[1]);
        }

        if ($("#fltParken").is(':checked')) {
            setPolygons(jsonLinks[2]);
        }

        if ($("#fltWinkel").is(':checked')) {
            setMultiLinks(jsonLinks[3]);
        }

        if ($("#fltSport").is(':checked')) {
            setMarkers(jsonLinks[4]);
        }

        mapLoader.fadeOut();

        return false;
    });

    // we creeeren een prototype functie om te berekenen of de coordinaten in de cirkel liggen
    google.maps.Circle.prototype.contains = function(latLng) {
        return this.getBounds().contains(latLng) && google.maps.geometry.spherical.computeDistanceBetween(this.getCenter(), latLng) <= this.getRadius();
    };

    // populate de jsonLinks array
    pushJSONLinks();

    // locatie van de gebruiker op de map
    geolocateUser();

});
