/**
 * Created by Artuur on 9/08/16.
 */
    var Quiz = function(){
        var self = this;
        this.init = function(){
            self._bindEvents();
        };

        this.correctAnswers = [
            { question: 1, answer: 'c' },
            { question: 2, answer: 'd' },
            { question: 3, answer: 'b' },
            { question: 4, answer: 'a' },
            { question: 5, answer: 'b' }
        ];

        this._pickAnswer = function($answer, $answers){
            $answers.find('.quiz-answer').removeClass('active');
            $answer.addClass('active');
        };
        this._calcResult = function(){
            var numberOfCorrectAnswers = 0;
            $('ul[data-quiz-question]').each(function(i){
                var $this = $(this),
                    chosenAnswer = $this.find('.quiz-answer.active').data('quiz-answer'),
                    correctAnswer;

                for ( var j = 0; j < self.correctAnswers.length; j++ ) {
                    var a = self.correctAnswers[j];
                    if ( a.question == $this.data('quiz-question') ) {
                        correctAnswer = a.answer;
                    }
                }

                if ( chosenAnswer == correctAnswer ) {
                    numberOfCorrectAnswers++;

                    // highlight this as correct answer
                    $this.find('.quiz-answer.active').addClass('correct');
                }
                else {
                    $this.find('.quiz-answer[data-quiz-answer="'+correctAnswer+'"]').addClass('correct');
                    $this.find('.quiz-answer.active').addClass('incorrect');
                }
            });
            if ( numberOfCorrectAnswers < 2 ) {
                return {code: 'bad', text: 'Aiaiai...nog wat werk aan de winkel! Gebruik WHADOO meer!'};
            }
            else if ( numberOfCorrectAnswers == 3 ) {
                return {code: 'mid', text: 'WHADOO nog ietwat intensiever gebruiken is de tip!'};
            }
            else if ( numberOfCorrectAnswers >= 4 ) {
                return {code: 'good', text: 'Wahaw! Prachtig! WHADOO-addict?'};
            }
        };
        this._isComplete = function(){
            var answersComplete = 0;
            $('ul[data-quiz-question]').each(function(){
                if ( $(this).find('.quiz-answer.active').length ) {
                    answersComplete++;
                }
            });
            if ( answersComplete >= 5 ) {
                return true;
            }
            else {
                return false;
            }
        };
        this._showResult = function(result){
            $('.quiz-result').addClass(result.code).html(result.text);
        };
        this._bindEvents = function(){
            $('.quiz-answer').on('click', function(){
                var $this = $(this),
                    $answers = $this.closest('ul[data-quiz-question]');
                self._pickAnswer($this, $answers);
                if ( self._isComplete() ) {

                    // scroll to answer section
                    $('html, body').animate({
                        scrollTop: $('.quiz-result').offset().top
                    });

                    self._showResult( self._calcResult() );
                    $('.quiz-answer').off('click');

                }
            });
        };
        this._reset = function(){
            $('.quiz-answer').removeClass('active correct incorrect');
            $('.quiz-result').removeClass('bad good mid');
            self._bindEvents();
        };
    };
    var quiz = new Quiz();
    quiz.init();
